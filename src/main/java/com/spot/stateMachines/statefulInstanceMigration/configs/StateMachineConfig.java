package com.spot.stateMachines.statefulInstanceMigration.configs;

import com.spot.stateMachines.statefulInstanceMigration.stateMachine.action.SICreateImageInAWSAction;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.action.SICreateImageInDBAction;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.action.SIMigrationStartAction;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.action.SIProtectInstanceFromTerminationAction;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.action.StayInSameStateAction;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard.SICreateImageInAWSGuard;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard.SICreateImageInDBGuard;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard.SIProtectInstanceFromTerminationGuard;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard.StayInSameStateGuard;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.listener.SIMigrationListener;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import java.time.Duration;
import java.util.EnumSet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.persist.StateMachineRuntimePersister;
import org.springframework.statemachine.service.DefaultStateMachineService;
import org.springframework.statemachine.service.StateMachineService;

@Slf4j
@Configuration
@EnableStateMachineFactory
@RequiredArgsConstructor
public class StateMachineConfig extends
        EnumStateMachineConfigurerAdapter<SIMigrationState, SIMigrationEvent> {

    private SIMigrationStartAction migrationStartAction;
    private SIProtectInstanceFromTerminationAction protectInstanceFromTerminationAction;
    private SICreateImageInAWSAction createImageInAWSAction;
    private SICreateImageInDBAction createImageInDBAction;
    private StayInSameStateAction stayInSameStateAction;

    private SIProtectInstanceFromTerminationGuard protectInstanceFromTerminationGuard;
    private SICreateImageInAWSGuard createImageInAWSGuard;
    private SICreateImageInDBGuard createImageInDBGuard;

    private final StateMachineRuntimePersister<SIMigrationState, SIMigrationEvent, String> stateMachineRuntimePersister;

    @Autowired
    public void setSiMigrationStartAction(SIMigrationStartAction migrationStartAction) {
        this.migrationStartAction = migrationStartAction;
    }

    @Autowired
    public void setProtectInstanceFromTerminationAction(
            SIProtectInstanceFromTerminationAction protectInstanceFromTerminationAction) {
        this.protectInstanceFromTerminationAction = protectInstanceFromTerminationAction;
    }

    @Autowired
    public void setCreateImageInAWSAction(
            SICreateImageInAWSAction createImageInAWSAction) {
        this.createImageInAWSAction = createImageInAWSAction;
    }

    @Autowired
    public void setCreateImageInDBAction(
            SICreateImageInDBAction createImageInDBAction) {
        this.createImageInDBAction = createImageInDBAction;
    }

    @Autowired
    public void setSIStayInSameStateAction(
            StayInSameStateAction stayInSameStateAction) {
        this.stayInSameStateAction = stayInSameStateAction;
    }

    @Autowired
    public void setProtectInstanceFromTerminationGuard(
            SIProtectInstanceFromTerminationGuard protectInstanceFromTerminationGuard) {
        this.protectInstanceFromTerminationGuard = protectInstanceFromTerminationGuard;
    }

    @Autowired
    public void setCreateImageInAWSGuard(
            SICreateImageInAWSGuard createImageInAWSGuard) {
        this.createImageInAWSGuard = createImageInAWSGuard;
    }

    @Autowired
    public void setCreateImageInDBGuard(
            SICreateImageInDBGuard createImageInDBGuard) {
        this.createImageInDBGuard = createImageInDBGuard;
    }

    @Override
    public void configure(StateMachineStateConfigurer<SIMigrationState, SIMigrationEvent> states)
            throws Exception {
        states.withStates()
              .initial(SIMigrationState.START, migrationStartAction)
              .junction(SIMigrationState.IS_PREPARED_AWS_INSTANCE)
              .junction(SIMigrationState.IS_IMAGE_CREATED_IN_AWS)
              .junction(SIMigrationState.IS_IMAGE_CREATED_IN_DB)
              .end(SIMigrationState.FINISH)
              .states(EnumSet.allOf(SIMigrationState.class));
    }

    @Override
    public void configure(
            StateMachineConfigurationConfigurer<SIMigrationState, SIMigrationEvent> config)
            throws Exception {
        var taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(10);
        taskExecutor.initialize();

        config.withPersistence()
              .runtimePersister(stateMachineRuntimePersister);

        config.withConfiguration()
              .taskExecutor(taskExecutor)
              .listener(new SIMigrationListener());
    }

    @Override
    public void configure(
            StateMachineTransitionConfigurer<SIMigrationState, SIMigrationEvent> transitions)
            throws Exception {

        transitions.withExternal()
                   .source(SIMigrationState.START)
                   .target(SIMigrationState.PREPARE_AWS_INSTANCE)
                   .action(protectInstanceFromTerminationAction)

                   .and()
                   .withExternal()
                   .source(SIMigrationState.PREPARE_AWS_INSTANCE)
                   .target(SIMigrationState.IS_PREPARED_AWS_INSTANCE)
                   .timerOnce(Duration.ofSeconds(5)
                                      .toMillis()) //give a time for protect the instance

                   .and()
                   .withJunction()
                   .source(SIMigrationState.IS_PREPARED_AWS_INSTANCE)
                   .first(SIMigrationState.CREATE_IMAGE, protectInstanceFromTerminationGuard,
                           createImageInAWSAction)
                   .then(SIMigrationState.PREPARE_AWS_INSTANCE_TIMEOUT,
                           new StayInSameStateGuard(Duration.ofSeconds(10)), stayInSameStateAction)
                   .last(SIMigrationState.ERROR)

                   .and()
                   .withExternal()
                   .source(SIMigrationState.PREPARE_AWS_INSTANCE_TIMEOUT)
                   .timerOnce(5000)
                   .target(SIMigrationState.IS_PREPARED_AWS_INSTANCE)
                   .action(protectInstanceFromTerminationAction)

                   .and()
                   .withExternal()
                   .source(SIMigrationState.CREATE_IMAGE)
                   .target(SIMigrationState.IS_IMAGE_CREATED_IN_AWS)
                   .timerOnce(Duration.ofSeconds(5)
                                      .toMillis()) //give a time for create an image

                   .and()
                   .withJunction()
                   .source(SIMigrationState.IS_IMAGE_CREATED_IN_AWS)
                   .first(SIMigrationState.IS_IMAGE_CREATED_IN_DB, createImageInAWSGuard,
                           createImageInDBAction)
                   .then(SIMigrationState.CREATE_IMAGE_TIMEOUT,
                           new StayInSameStateGuard(Duration.ofSeconds(10)), stayInSameStateAction)
                   .last(SIMigrationState.ERROR)

                   .and()
                   .withJunction()
                   .source(SIMigrationState.IS_IMAGE_CREATED_IN_DB)
                   .first(SIMigrationState.AWAIT_USER_ACTION, createImageInDBGuard)
                   .then(SIMigrationState.CREATE_IMAGE_TIMEOUT,
                           new StayInSameStateGuard(Duration.ofSeconds(10)), stayInSameStateAction)
                   .last(SIMigrationState.ERROR)

                   .and()
                   .withExternal()
                   .source(SIMigrationState.CREATE_IMAGE_TIMEOUT)
                   .timerOnce(5000)
                   .target(SIMigrationState.IS_IMAGE_CREATED_IN_AWS)
                   .action(createImageInAWSAction)
                   .action(createImageInDBAction)

                   .and()
                   .withExternal()
                   .source(SIMigrationState.AWAIT_USER_ACTION)
                   .target(SIMigrationState.FINISH)
                   .event(SIMigrationEvent.USER_ACTION)
                   .action(context -> log.info("Getting user action and finish"));

    }

    @Bean
    public StateMachineService<SIMigrationState, SIMigrationEvent> stateMachineService(
            StateMachineFactory<SIMigrationState, SIMigrationEvent> stateMachineFactory,
            StateMachineRuntimePersister<SIMigrationState, SIMigrationEvent, String> stateMachineRuntimePersister) {
        return new DefaultStateMachineService<>(stateMachineFactory,
                stateMachineRuntimePersister);
    }
}
