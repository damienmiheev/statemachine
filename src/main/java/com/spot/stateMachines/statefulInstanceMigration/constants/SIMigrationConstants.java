package com.spot.stateMachines.statefulInstanceMigration.constants;

import lombok.experimental.UtilityClass;

@UtilityClass
public class SIMigrationConstants {

    public String MIGRATION_ID = "MIGRATION_ID";
    public String INSTANCE_ID = "INSTANCE_ID";
    public String GROUP_ID = "GROUP_ID";
    public String ACCOUNT_ID = "ACCOUNT_ID";
    public String IS_PROTECTED_FROM_TERMINATION = "IS_PROTECTED_FROM_TERMINATION";
    public String IN_CURRENT_STATE_START_DATE = "IN_CURRENT_STATE_START_DATE";
    public String IS_IMAGE_CREATED_IN_AWS = "IS_IMAGE_CREATED_IN_AWS";
    public String IS_IMAGE_CREATED_IN_DB = "IS_IMAGE_CREATED_IN_DB";
}
