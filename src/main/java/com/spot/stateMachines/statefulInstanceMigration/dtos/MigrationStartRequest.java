package com.spot.stateMachines.statefulInstanceMigration.dtos;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class MigrationStartRequest {

    @NotBlank
    private String accountId;

    @NotBlank
    private String migrationId;

    @NotBlank
    private String instanceId;

    @NotBlank
    private String groupId;
}
