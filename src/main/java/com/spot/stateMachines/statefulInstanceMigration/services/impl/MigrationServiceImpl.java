package com.spot.stateMachines.statefulInstanceMigration.services.impl;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import com.spot.stateMachines.statefulInstanceMigration.dtos.MigrationStartRequest;
import com.spot.stateMachines.statefulInstanceMigration.services.MigrationService;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.service.StateMachineService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MigrationServiceImpl implements MigrationService {

    private final StateMachineFactory<SIMigrationState, SIMigrationEvent> stateMachineFactory;
    private final StateMachineService<SIMigrationState, SIMigrationEvent> stateMachineService;

    @Override
    public void start(MigrationStartRequest migrationStartRequest) {
        var migrationId = migrationStartRequest.getMigrationId();
        var instanceId = migrationStartRequest.getInstanceId();
        var groupId = migrationStartRequest.getGroupId();
        var accountId = migrationStartRequest.getAccountId();

        log.info("Handling migration: {}, original instance id: {}, group: {}", migrationId,
                instanceId, groupId);

        var stateMachine = stateMachineFactory.getStateMachine(migrationId);

        var variables = stateMachine.getExtendedState()
                                    .getVariables();

        variables.put(SIMigrationConstants.MIGRATION_ID, migrationId);
        variables.put(SIMigrationConstants.INSTANCE_ID, instanceId);
        variables.put(SIMigrationConstants.GROUP_ID, groupId);
        variables.put(SIMigrationConstants.ACCOUNT_ID, accountId);

        stateMachine.start();
    }

    @Override
    public boolean action(String id) {
        var stateMachine = stateMachineService.acquireStateMachine(id);

        stateMachine.sendEvent(SIMigrationEvent.USER_ACTION);
        return true;
    }
}
