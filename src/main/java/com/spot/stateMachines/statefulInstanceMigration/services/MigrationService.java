package com.spot.stateMachines.statefulInstanceMigration.services;

import com.spot.stateMachines.statefulInstanceMigration.dtos.MigrationStartRequest;

public interface MigrationService {

    void start(MigrationStartRequest migrationStartRequest);

    boolean action(String id);
}
