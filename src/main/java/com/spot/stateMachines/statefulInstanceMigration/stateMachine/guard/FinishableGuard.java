package com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard;

import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

public abstract class FinishableGuard implements Guard<SIMigrationState, SIMigrationEvent> {

    protected abstract String finishKey();

    @Override
    public boolean evaluate(StateContext<SIMigrationState, SIMigrationEvent> context) {
        var retVal = context.getExtendedState()
                            .get(finishKey(), Boolean.class);
        return retVal != null && retVal;
    }

}
