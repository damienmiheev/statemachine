package com.spot.stateMachines.statefulInstanceMigration.stateMachine.state;

public enum SIMigrationState {
    START,

    PREPARE_AWS_INSTANCE,
    IS_PREPARED_AWS_INSTANCE,
    PREPARE_AWS_INSTANCE_TIMEOUT,

    CREATE_IMAGE,
    IS_IMAGE_CREATED_IN_AWS,
    IS_IMAGE_CREATED_IN_DB,
    CREATE_IMAGE_TIMEOUT,

    AWAIT_FOR_IMAGE,
    AWAIT_USER_ACTION,
    TERMINATE_INSTANCE,
    AWAIT_INSTANCE_TERMINATION,
    UPDATE_GROUP,
    LAUNCH_INSTANCE,
    RESUME_STATEFUL_INSTANCE,
    AWAIT_INSTANCE_RUNNING,
    AWAIT_STATEFUL_INSTANCE_RUNNING,
    CANCEL,
    FINISH,
    ERROR
}
