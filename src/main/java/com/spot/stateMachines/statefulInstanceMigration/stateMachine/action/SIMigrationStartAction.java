package com.spot.stateMachines.statefulInstanceMigration.stateMachine.action;

import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SIMigrationStartAction implements Action<SIMigrationState, SIMigrationEvent> {

    @SneakyThrows
    @Override
    public void execute(StateContext<SIMigrationState, SIMigrationEvent> context) {
        Thread.sleep(500);
        log.info("Stateful migration start event created");
        log.info("Migration started, moving on to preparing aws instance for import");
    }
}
