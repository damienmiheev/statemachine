package com.spot.stateMachines.statefulInstanceMigration.stateMachine.action;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import java.time.Duration;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.trigger.TimerTrigger;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StayInSameStateAction implements Action<SIMigrationState, SIMigrationEvent> {

    @Override
    public void execute(StateContext<SIMigrationState, SIMigrationEvent> context) {
        var stateName = context.getSource()
                               .getId();
        var inCurrentStateStartDateKey = new StringBuilder(
                SIMigrationConstants.IN_CURRENT_STATE_START_DATE).append("_")
                                                                 .append(stateName);

        var extendedState = context.getExtendedState();
        var inSameStateStartDate = extendedState.get(inCurrentStateStartDateKey,
                LocalDateTime.class);

        if (inSameStateStartDate == null) {
            //first iteration
            inSameStateStartDate = LocalDateTime.now();
            extendedState
                    .getVariables()
                    .put(inCurrentStateStartDateKey, inSameStateStartDate);
        } else {
            var trigger = context.getTransition()
                                 .getTrigger();

            if (trigger instanceof TimerTrigger) {
                var timerTrigger = ((TimerTrigger<SIMigrationState, SIMigrationEvent>) trigger);
                var period = timerTrigger.getPeriod();
                var periodInSeconds = Duration.ofMillis(period)
                                              .getSeconds();

                log.info("Staying in state {} for {}", stateName, periodInSeconds);
            }
        }
    }
}
