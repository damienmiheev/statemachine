package com.spot.stateMachines.statefulInstanceMigration.stateMachine.action;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SICreateImageInDBAction extends RepeatableAction {

    @SneakyThrows
    @Override
    public void execute(StateContext<SIMigrationState, SIMigrationEvent> context) {
        var extendedState = context.getExtendedState();
        var instanceId = extendedState.get(SIMigrationConstants.INSTANCE_ID, String.class);

        var isImageCreatedInDb = extendedState.get(SIMigrationConstants.IS_IMAGE_CREATED_IN_DB,
                Boolean.class);

        if (isImageCreatedInDb == null || !isImageCreatedInDb) {

            var repeatCounter = getRepeatCounter(context);

            //simulate creating image in aws
            var count = repeatCounter.getAndIncrement();
            Thread.sleep(500);

            if (count < 2) {
                log.error("Failed to create image in db for instance {}", instanceId);
            } else {
                log.info("Image for instance {} was successfully created in db", instanceId);

                context.getExtendedState()
                       .getVariables()
                       .put(SIMigrationConstants.IS_IMAGE_CREATED_IN_DB, true);
            }
        }
    }
}
