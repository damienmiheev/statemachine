package com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import org.springframework.stereotype.Component;

@Component
public class SIProtectInstanceFromTerminationGuard extends FinishableGuard {

    @Override
    protected String finishKey() {
        return SIMigrationConstants.IS_PROTECTED_FROM_TERMINATION;
    }
}
