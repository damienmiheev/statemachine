package com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import lombok.AllArgsConstructor;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.guard.Guard;

@AllArgsConstructor
public class StayInSameStateGuard implements Guard<SIMigrationState, SIMigrationEvent> {

    private final Duration stateFailThreshold;

    @Override
    public boolean evaluate(StateContext<SIMigrationState, SIMigrationEvent> context) {
        var stateName = context.getSource()
                               .getId();
        var inCurrentStateStartDateKey = new StringBuilder(
                SIMigrationConstants.IN_CURRENT_STATE_START_DATE).append("_")
                                                                 .append(stateName);

        var now = LocalDateTime.now();
        var inSameStateDate = context.getExtendedState()
                                     .get(inCurrentStateStartDateKey, LocalDateTime.class);
        return inSameStateDate == null
                || ChronoUnit.SECONDS.between(inSameStateDate, now)
                < stateFailThreshold.getSeconds();

    }
}
