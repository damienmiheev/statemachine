package com.spot.stateMachines.statefulInstanceMigration.stateMachine.action;

import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;

public abstract class RepeatableAction implements Action<SIMigrationState, SIMigrationEvent> {

    private final Map<UUID, AtomicInteger> repeatableHolder = new HashMap<>();

    protected AtomicInteger getRepeatCounter(
            StateContext<SIMigrationState, SIMigrationEvent> context) {
        var stateMachineId = context.getStateMachine()
                                    .getUuid();

        return repeatableHolder.computeIfAbsent(stateMachineId, k -> new AtomicInteger());
    }
}
