package com.spot.stateMachines.statefulInstanceMigration.stateMachine.listener;

import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

@Slf4j
public class SIMigrationListener extends
        StateMachineListenerAdapter<SIMigrationState, SIMigrationEvent> {

    @Override
    public void stateContext(StateContext<SIMigrationState, SIMigrationEvent> stateContext) {
        super.stateContext(stateContext);
    }

    @Override
    public void stateEntered(State<SIMigrationState, SIMigrationEvent> state) {
        log.info("Current state is {}", state.getId());
    }
}
