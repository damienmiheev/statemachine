package com.spot.stateMachines.statefulInstanceMigration.stateMachine.action;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SIProtectInstanceFromTerminationAction extends RepeatableAction {

    @SneakyThrows
    @Override
    public void execute(StateContext<SIMigrationState, SIMigrationEvent> context) {
        var instanceId = context.getExtendedState()
                                .get(SIMigrationConstants.INSTANCE_ID, String.class);

        var repeatCounter = getRepeatCounter(context);

        //simulate updating instance in aws
        var count = repeatCounter.getAndIncrement();
        Thread.sleep(500);

        if (count < 3) {
            log.error("Failed to set disableApiTermination to true for instance {}", instanceId);
        } else {
            log.info("Instance {} was successfully updated and is now protected from termination",
                    instanceId);

            context.getExtendedState()
                   .getVariables()
                   .put(SIMigrationConstants.IS_PROTECTED_FROM_TERMINATION, true);
        }

    }
}
