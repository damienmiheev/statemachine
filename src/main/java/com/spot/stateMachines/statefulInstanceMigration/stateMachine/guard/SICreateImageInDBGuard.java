package com.spot.stateMachines.statefulInstanceMigration.stateMachine.guard;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import org.springframework.stereotype.Component;

@Component
public class SICreateImageInDBGuard extends FinishableGuard {

    @Override
    protected String finishKey() {
        return SIMigrationConstants.IS_IMAGE_CREATED_IN_DB;
    }
}
