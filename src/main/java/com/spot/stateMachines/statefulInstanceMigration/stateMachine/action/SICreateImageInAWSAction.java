package com.spot.stateMachines.statefulInstanceMigration.stateMachine.action;

import com.spot.stateMachines.statefulInstanceMigration.constants.SIMigrationConstants;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.event.SIMigrationEvent;
import com.spot.stateMachines.statefulInstanceMigration.stateMachine.state.SIMigrationState;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SICreateImageInAWSAction extends RepeatableAction {

    @SneakyThrows
    @Override
    public void execute(StateContext<SIMigrationState, SIMigrationEvent> context) {
        var extendedState = context.getExtendedState();
        var instanceId = extendedState.get(SIMigrationConstants.INSTANCE_ID, String.class);

        var isImageCreatedInAws = extendedState.get(SIMigrationConstants.IS_IMAGE_CREATED_IN_AWS,
                Boolean.class);

        if (isImageCreatedInAws == null || !isImageCreatedInAws) {

            var repeatCounter = getRepeatCounter(context);

            //simulate creating image in aws
            var count = repeatCounter.getAndIncrement();
            Thread.sleep(500);

            if (count < 2) {
                log.error("Failed to create image in aws for instance {}", instanceId);
            } else {
                log.info("Image for instance {} was successfully created in aws", instanceId);

                extendedState
                        .getVariables()
                        .put(SIMigrationConstants.IS_IMAGE_CREATED_IN_AWS, true);
            }
        }
    }
}
