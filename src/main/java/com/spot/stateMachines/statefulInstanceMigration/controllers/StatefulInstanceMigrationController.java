package com.spot.stateMachines.statefulInstanceMigration.controllers;

import com.spot.stateMachines.statefulInstanceMigration.dtos.MigrationStartRequest;
import com.spot.stateMachines.statefulInstanceMigration.services.MigrationService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("statefulInstance/migration")
public class StatefulInstanceMigrationController {

    private final MigrationService migrationService;

    @PostMapping("start")
    public void start(@RequestBody @Valid MigrationStartRequest migrationStartRequest) {
        migrationService.start(migrationStartRequest);
    }

    @GetMapping("action")
    public boolean action(@RequestParam("id") String id) {
        return migrationService.action(id);
    }
}
